[![Downloads](https://static.pepy.tech/badge/jija_sqlalchemy)](https://pepy.tech/project/jija_sqlalchemy)
[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![PyPI version](https://badge.fury.io/py/jija_sqlalchemy.svg)](https://badge.fury.io/py/jija_sqlalchemy)


## Hello there
There is sqlalchemy driver for jija framework. It based on default jija database driver.
You can work with it in sync and async mode.


## Installation
In this example we will use postgresql database.

Install this driver:

    pip install jija_sqlalchemy

Install driver that will be used to connect to database in app:

    pip install asyncpg

Install driver that will be used to connect to database in migrations:

    pip install psycopg2

Or if you use linux:

    pip install psycopg2-binary


## Include driver
Fill driver in settings.py:

```python
from jija import config

from jija_sqlalchemy import SQLAlchemyDriver
from sqlalchemy.ext.asyncio import create_async_engine

...

config.DriversConfig(
    database=SQLAlchemyDriver(
        engine=create_async_engine('postgresql+asyncpg://<user>:<password>@<host>:<port>/<database>'),
        migrator_url='postgresql://<user>:<password>@<host>:<port>/<database>',
    )
)
```


## Create models
Create models.py in project root directory and add base model to it,
you can add any fields if you want.

```python
from sqlalchemy.orm import DeclarativeBase


class Base(DeclarativeBase):
    pass
```

Create models.py in app directory and add models to it.

```python
from models import Base
from sqlalchemy import Column, Integer, String


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
```


## Create migrations
Execute this command in terminal:
    
    python main.py migrate

First run will only create migrations folder for alembic.

Then you need to edit `migrations/env.py` file. Find `target_metadata = None` and replace it with this:

```python
...
from models import Base
target_metadata = Base.metadata
...
```

Then execute migration command one more time:

    python main.py migrate


Check that `migrations/versions` folder will have migration file.


## Use migrations
To use migrations you need to execute this command:

    python main.py upgrade


## Use in views
```python
async with config.DriversConfig.DATABASE.engine.connect() as conn:
    for user in await conn.execute(select(models.User)):
        # do something with user
```

## Jija
[Documentation](https://jija.readthedocs.io/en/latest/)

[Repository](https://gitlab.com/by_dezz/jija)
